package com.atlassian.plugins.textmarkup;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.web.ContextProvider;
import com.google.common.collect.Maps;
import java.util.Map;

@Scanned
public class TextMarkupContextProvider implements ContextProvider {

    private final PluginAccessor accessor;

    public TextMarkupContextProvider(@ComponentImport final PluginAccessor accessor) {
        this.accessor = accessor;
    }

    @Override
    public void init(final Map<String, String> params) throws PluginParseException {}

    @Override
    public Map<String, Object> getContextMap(final Map<String, Object> context) {
        final Plugin plugin = accessor.getEnabledPlugin("com.atlassian.plugins.textmarkup");

        final Map<String, Object> ctx = Maps.newHashMap(context);
        ctx.put("version", plugin.getPluginInformation().getVersion());
        ctx.put("pluginName", plugin.getName());

        return ctx;
    }
}

package com.atlassian.plugins.textmarkup;

import com.atlassian.jira.issue.RendererManager;
import com.atlassian.jira.issue.fields.renderer.JiraRendererPlugin;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

@Scanned
@Path("/render")
@Consumes({ MediaType.TEXT_PLAIN })
@Produces({ MediaType.TEXT_PLAIN })
public class Resource {

    private final RendererManager rendererManager;

    public Resource(@ComponentImport final RendererManager rendererManager) {
        this.rendererManager = rendererManager;
    }

    @POST
    @AnonymousAllowed
    public String markupRender(final String input) {
        JiraRendererPlugin renderer = this.rendererManager.getRendererForType("atlassian-wiki-renderer");
        return (renderer == null) ? "" : renderer.render(input, null);
    }
}

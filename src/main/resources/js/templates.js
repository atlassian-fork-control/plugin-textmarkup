define('jira-dashboard-items/textmarkup-templates', function() {
    'use strict';

    return Object.freeze({
        main: TextMarkup.Templates.Main,
        config: TextMarkup.Templates.Config,
        preview: TextMarkup.Templates.Preview
    });
});

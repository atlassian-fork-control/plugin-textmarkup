AJS.test.require([
    'com.atlassian.plugins.textmarkup:textmarkup-resources'
], function() {
    'use strict';

    var utils = require('jira-dashboard-items/textmarkup-utils');
    var templates = require('jira-dashboard-items/textmarkup-templates');
    var ok = QUnit.assert.ok.bind(QUnit.assert);

    module('Text Markdown', function() {

        module('utils', function() {

            test('createElementDiv', function() {
                var node = utils.createElementDiv();
                equal(!!node && node.tagName === 'DIV', true);
            });

            test('populate', function() {
                var html = '<div></div>';
                equal(utils.populate(utils.createElementDiv(), html).innerHTML, html);
                equal(utils.populate(utils.createElementDiv(), html + html).innerHTML, html + html);
            });

            test('sandbox', function() {
                equal(utils.sandbox('<p></p>').firstElementChild.tagName, 'P');
                equal(utils.sandbox('<script>').firstElementChild.tagName, 'SCRIPT');
                equal(utils.sandbox('<script></script>').firstElementChild.tagName, 'SCRIPT');
            });

            test('isString', function () {
                ok(utils.isString(''));
                ok(utils.isString('asd'));
                ok(!utils.isString(5));
            });

            test('hasTagsScript', function () {
                ok(utils.hasTagsScript('<script></script>'));
                ok(utils.hasTagsScript('<script>'));
                ok(!utils.hasTagsScript('asd'));
                ok(!utils.hasTagsScript(123));
            });

            test('hasTagsHTML', function () {
                ok(utils.hasTagsHTML('<p></p>'));
                ok(utils.hasTagsHTML('<script></script>'));
                ok(utils.hasTagsHTML('<script>'));
                ok(!utils.hasTagsHTML('123'));
                ok(!utils.hasTagsHTML(123));
            });

            module('eventsFactory', function () {

                test('propagates', function() {
                    var events = utils.eventsFactory();
                    var node = utils.createElementDiv();
                    var counter = 0;

                    events.on(node, 'click', function listener() {
                        counter += 1;
                    });
                    node.click();
                    equal(counter, 1);

                    events.off();
                });

                test('does not propagate', function() {
                    var events = utils.eventsFactory();
                    var node = utils.createElementDiv();
                    var counter = 0;

                    events.on(node, 'click', function listener() {
                        counter += 1;
                    });
                    events.off();
                    node.click();
                    equal(counter, 0);
                });

            });

            module('findFactory', function () {
                var subview = 'test';

                test('found', function () {
                    var root = utils.createElementDiv();
                    var child = root.appendChild(utils.createElementDiv());
                    child.id = utils.PREFIX + subview + '-a';

                    equal(utils.findFactory(root, subview)('a'), child);
                });

                test('not found', function () {
                    var root = utils.createElementDiv();

                    equal(utils.findFactory(root, subview)('a'), null);
                });
            });

            test('baseErrorsFind', function() {
                var root = utils.populate(utils.createElementDiv(), templates.main());
                var find = utils.findFactory(root, 'base');
                var errors = utils.baseErrorsFind(find);

                ok(errors.markupEmpty);
                ok(errors.markupEmptyPreview);
                ok(errors.markupTagsHTML);
                ok(errors.markupTagsScript);
            });

            test('configInputsFind', function() {
                var root = utils.populate(utils.createElementDiv(), templates.config());
                var find = utils.findFactory(root, 'config');
                var inputs = utils.configInputsFind(find);

                ok(inputs.title);
                ok(inputs.content);
            });

            test('configErrorsFind', function() {
                var root = utils.populate(utils.createElementDiv(), templates.config());
                var find = utils.findFactory(root, 'config');
                var errors = utils.configErrorsFind(find);

                ok(errors.title);
                ok(errors.filled);
                ok(errors.html);
                ok(errors.markupEmpty);
                ok(errors.markupEmptyPreview);
                ok(errors.markupTagsHTML);
                ok(errors.markupTagsScript);
            });

            test('configButtonsFind', function() {
                var root = utils.populate(utils.createElementDiv(), templates.config());
                var find = utils.findFactory(root, 'config');
                var buttons = utils.configButtonsFind(find);

                ok(buttons.save);
                ok(buttons.preview);
                ok(buttons.cancel);
            });

            module('errorsMarkupDisplay', function() {
                function errorsGenerate() {
                    var root = utils.populate(utils.createElementDiv(), templates.main());
                    var find = utils.findFactory(root, 'base');
                    return utils.baseErrorsFind(find);
                }

                test('empty', function() {
                    var errors = errorsGenerate();
                    utils.errorsMarkupDisplay('', errors, false);

                    equal(errors.markupEmpty.classList.contains('hidden'), false);
                    equal(errors.markupTagsHTML.classList.contains('hidden'), true);
                    equal(errors.markupTagsScript.classList.contains('hidden'), true);
                });

                test('not string', function() {
                    var errors = errorsGenerate();
                    utils.errorsMarkupDisplay(undefined, errors, false);

                    equal(errors.markupEmpty.classList.contains('hidden'), false);
                    equal(errors.markupTagsHTML.classList.contains('hidden'), true);
                    equal(errors.markupTagsScript.classList.contains('hidden'), true);
                });

                test('tags html (checked)', function() {
                    var errors = errorsGenerate();
                    utils.errorsMarkupDisplay('<p>', errors, true);

                    equal(errors.markupEmpty.classList.contains('hidden'), true);
                    equal(errors.markupTagsHTML.classList.contains('hidden'), false);
                    equal(errors.markupTagsScript.classList.contains('hidden'), true);
                });

                test('tags html (not checked)', function() {
                    var errors = errorsGenerate();
                    utils.errorsMarkupDisplay('<p>', errors, false);

                    equal(errors.markupEmpty.classList.contains('hidden'), true);
                    equal(errors.markupTagsHTML.classList.contains('hidden'), true);
                    equal(errors.markupTagsScript.classList.contains('hidden'), true);
                });

                test('tags script (open)', function() {
                    var errors = errorsGenerate();
                    utils.errorsMarkupDisplay('<script>', errors, false);

                    equal(errors.markupEmpty.classList.contains('hidden'), true);
                    equal(errors.markupTagsHTML.classList.contains('hidden'), true);
                    equal(errors.markupTagsScript.classList.contains('hidden'), false);
                });

                test('tags script (closed)', function() {
                    var errors = errorsGenerate();
                    utils.errorsMarkupDisplay('<script></script>', errors, false);

                    equal(errors.markupEmpty.classList.contains('hidden'), true);
                    equal(errors.markupTagsHTML.classList.contains('hidden'), true);
                    equal(errors.markupTagsScript.classList.contains('hidden'), false);
                });

            });

        });

    });

});

define('jira-dashboard-items/textmarkup', [
    'jira-dashboard-items/textmarkup-templates',
    'jira-dashboard-items/textmarkup-utils',
    'jira/util/logger',
    'aui/dialog2',
    'jquery'
], function(templates, utils, logger, dialog, $) {
    'use strict';

    function Gadget(API, options) {
        this.API = API;
        this.options = options;

        Object.freeze(this);
    }

    Gadget.prototype.render = function(element, preferences) {
        this.API.setTitle(preferences.title);

        var gadget = this;
        var markup = preferences.content;
        var root = utils.populate(element[0], templates.main());
        var find = utils.findFactory(root, 'base');

        if (markup && utils.isString(markup) && !utils.hasTagsHTML(markup)) {
            gadget.API.showLoadingBar();
            utils.renderRequest(preferences.content)
                .done(function(markup) {
                    gadget.API.hideLoadingBar();

                    if (markup && utils.isString(markup) && !utils.hasTagsScript(markup)) {
                        find('view').innerHTML = markup;
                        gadget.resize();
                    } else if (!gadget.API.isEditable()) {
                        utils.errorsMarkupDisplay(markup, utils.baseErrorsFind(find), false);
                        gadget.resize();
                    } else {
                        gadget.renderEdit(element, preferences);
                    }
                });
        } else if (!gadget.API.isEditable()) {
            utils.errorsMarkupDisplay(markup, utils.baseErrorsFind(find), true);
            gadget.resize();
        } else {
            gadget.renderEdit(element, preferences);
        }
    };

    Gadget.prototype.renderEdit = function(element, preferences) {
        var gadget = this;
        var root = utils.populate(element[0], templates.config());
        var find = utils.findFactory(root, 'config');
        var buttons = utils.configButtonsFind(find);
        var errors = utils.configErrorsFind(find);
        var inputs = utils.configInputsFind(find);
        var form = find('form');
        var events = utils.eventsFactory();
        var markup = preferences.content;

        inputs.title.value = preferences.title || '';

        if (utils.hasTagsScript(markup)) {
            inputs.content.value = preferences.content;
            errors.markupTagsScript.classList.remove('hidden');
        } else if (utils.hasTagsHTML(markup)) {
            inputs.content.value = preferences.content;
            errors.markupTagsHTML.classList.remove('hidden');
        } else {
            inputs.content.value = preferences.content || '';
        }

        events.on(form, 'submit', function submit(event) {
            event.preventDefault();
            return false;
        });
        events.on(inputs.content, 'resize', function resize() {
            gadget.API.resize();
        });
        events.on(buttons.save, 'click', function save(event) {
            if (utils.errorsCheck(gadget, inputs, errors)) {
                return;
            }

            // gadget.API.savePreferences automatically closes configuration view / 'reroutes' to render()
            gadget.API
                .savePreferences({
                    title: inputs.title.value.trim(),
                    content: inputs.content.value.trim()
                })
                .done(events.off)
                .error(logger.error);
        });
        events.on(buttons.preview, 'click', function preview() {
            if (utils.errorsCheck(gadget, inputs, errors)) {
                return;
            }

            utils.renderRequest(inputs.content.value).done(function(html) {
                if (!html || !utils.isString(html)) {
                    errors.markupEmptyPreview.classList.remove('hidden');
                } else if (utils.hasTagsScript(html)) {
                    errors.markupTagsScript.classList.remove('hidden');
                } else {
                    Object.keys(errors).forEach(function(key) {
                        $(errors[key]).toggleClass('hidden', true);
                    });

                    // we need to recreate the view every time, because AJS removes it for some reason
                    find('preview').innerHTML = templates.preview();
                    find('preview-content').innerHTML = html;
                    dialog('#demo-dialog').show();
                }

                gadget.resize();
            });
        });
        events.on(buttons.cancel, 'click', function cancel(event) {
            event.preventDefault();
            events.off();
            gadget.API.closeEdit();
        });

        if (!preferences.isConfigured || !markup || !utils.isString(markup)) {
            buttons.cancel.parentElement.removeChild(buttons.cancel);
        }

        gadget.resize();
    };

    Gadget.prototype.resize = function() {
        requestAnimationFrame(this.API.resize.bind(this.API));
    };

    Object.freeze(Gadget.prototype);

    return Gadget;
});

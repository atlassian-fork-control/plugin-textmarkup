define('jira-dashboard-items/textmarkup-utils', [
    'jquery',
    'wrm/context-path',
    'jira/util/logger'
], function($, contextPath, logger) {
    'use strict';

    var PREFIX = 'text-markdown-';

    function eventsFactory() {
        var list = [];

        return Object.freeze({
            on: function(node, event, listener) {
                node.addEventListener(event, listener);
                list.push(function() {
                    node.removeEventListener(event, listener);
                });
            },
            off: function() {
                list.forEach(function(fn) {
                    fn();
                });
                list.length = 0;
            }
        });
    }

    function findFactory(root, where) {
        return function find(what) {
            return root.querySelector('#' + PREFIX + where + '-' + what);
        };
    }

    function baseErrorsFind(find) {
        return Object.freeze({
            markupEmpty: find('markup-empty-error'),
            markupEmptyPreview: find('markup-empty-preview-error'),
            markupTagsHTML: find('markup-html-tags-error'),
            markupTagsScript: find('markup-script-tags-error')
        });
    }

    function configInputsFind(find) {
        return Object.freeze({
            title: find('title'),
            content: find('content')
        });
    }

    function configErrorsFind(find) {
        return Object.freeze(Object.assign({
            title: find('title-error'),
            filled: find('content-filled-error'),
            html: find('content-html-error')
        }, baseErrorsFind(find)));
    }

    function configButtonsFind(find) {
        return Object.freeze({
            save: find('buttons-save'),
            preview: find('buttons-preview'),
            cancel: find('buttons-cancel')
        });
    }

    function errorsCheck(gadget, inputs, errors) {
        var title = inputs.title.value.trim();
        var titleIsFilled = !!title && isString(title);
        var content = inputs.content.value.trim();
        var contentIsFilled = !!content && isString(content);
        var contentIsHTML = hasTagsHTML(content);

        $(errors.title).toggleClass('hidden', titleIsFilled);
        $(errors.filled).toggleClass('hidden', contentIsFilled);
        $(errors.html).toggleClass('hidden', !contentIsHTML);

        // adjust gadget's height in case of error messages
        gadget.resize();

        return !titleIsFilled || !contentIsFilled || contentIsHTML;
    }

    function sandbox(html) {
        return populate(createElementDiv(), html);
    }

    function populate(root, html) {
        root.innerHTML = html;
        return root;
    }

    function createElementDiv() {
        return document.createElement('div');
    }

    function errorsMarkupDisplay(markup, errors, priorityIsHTML) {
        // basically to do a generic html check you have to pass true for this 3rd argument
        if (!markup || !isString(markup)) {
            errors.markupEmpty.classList.remove('hidden');
        } else if (priorityIsHTML && hasTagsHTML(markup)) {
            errors.markupTagsHTML.classList.remove('hidden');
        } else if (hasTagsScript(markup)) {
            errors.markupTagsScript.classList.remove('hidden');
        }
    }

    function isString(value) {
        return typeof value === 'string';
    }

    function hasTagsScript(value) {
        return !!sandbox(value).querySelector('script');
    }

    function hasTagsHTML(value) {
        return !!sandbox(value).firstElementChild;
    }

    function renderRequest(input) {
        return $.ajax({
            method: "POST",
            url: contextPath() + "/rest/textmarkup/1/render",
            data: input.trim(),
            contentType: 'text/plain'
        }).error(logger.error);
    }

    return Object.freeze({
        PREFIX: PREFIX,
        renderRequest: renderRequest,
        eventsFactory: eventsFactory,
        findFactory: findFactory,
        errorsMarkupDisplay: errorsMarkupDisplay,
        baseErrorsFind: baseErrorsFind,
        configInputsFind: configInputsFind,
        configErrorsFind: configErrorsFind,
        configButtonsFind: configButtonsFind,
        errorsCheck: errorsCheck,
        isString: isString,
        hasTagsScript: hasTagsScript,
        hasTagsHTML: hasTagsHTML,
        createElementDiv: createElementDiv,
        sandbox: sandbox,
        populate: populate
    });
});
